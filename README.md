# DECP

[`decp.csv`](/decp.csv) converted from [`decp.xml`](/decp.xml) ([Fichier consolidé des données essentielles de la commande publique](https://www.data.gouv.fr/en/datasets/fichiers-consolides-des-donnees-essentielles-de-la-commande-publique/#_)) using [OpenRefine](https://openrefine.org/).
